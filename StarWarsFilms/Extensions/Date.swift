//
//  Date.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-04-02.
//

import Foundation


extension Date {
    
    static func getDate(_ day : Int, _ month: Int, _ year: Int) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        return formatter.date(from: "\(day)/\(month)/\(year) 00:00")
    }
}
