//
//  PreppingViews.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-03-28.
//

import Foundation
import UIKit


extension UIView {
    
    static func viewPreppedForAutoLayout() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}


extension UILabel {
    
    static func preppedForAutoLayout() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}

extension UICollectionView {
    
    static func preppedForAutoLayout() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
        
    }
}


extension UIImageView {
    
    static func preppedForAutoLayout() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}


extension UIButton {
    
    static func preppedForAutoLayout(type : UIButton.ButtonType = .custom) -> UIButton {
        let button = UIButton(type: type)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}
