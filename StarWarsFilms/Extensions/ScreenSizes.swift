//
//  ScreenSizes.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-04-02.
//

import Foundation
import UIKit

extension UIScreen {
    
    static var size : CGSize = .zero
    static var height : CGFloat { size.height }
    static var width : CGFloat { size.width }
}
