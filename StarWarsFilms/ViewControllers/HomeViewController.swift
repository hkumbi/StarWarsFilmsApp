//
//  HomeViewController.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-03-28.
//

import UIKit

class HomeViewController: UIViewController {
    
    typealias DataSource = UICollectionViewDiffableDataSource<MovieCategoryModel, MovieModel>
    typealias Snap = NSDiffableDataSourceSnapshot<MovieCategoryModel, MovieModel>
    
    let contentView = HomeView()
    let dateFormatter = DateFormatter()
    lazy var dataSource = makeDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        dateFormatter.dateFormat = "MMM dd, yyyy"
        configureCollectionView()
        makeStartingStaticData()
    }

    private func configureCollectionView() {
        contentView.collectionView.delegate = self
        contentView.collectionView.dataSource = dataSource
        contentView.collectionView.collectionViewLayout = makeLayout()
        contentView.collectionView.register(HomeMovieCollectionViewCell.self, forCellWithReuseIdentifier: HomeMovieCollectionViewCell.id)
        contentView.collectionView.register(MovieCategoryCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: MovieCategoryCollectionReusableView.id)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = HomeMovieCollectionViewCell.size
        layout.headerReferenceSize = MovieCategoryCollectionReusableView.size
        layout.minimumLineSpacing = 10
        layout.sectionInset = .init(top: 10, left: 0, bottom: 30, right: 0)
        return layout
    }
    
    private func makeStartingStaticData() {
        
        var snap = Snap()
        snap.appendSections(MovieCategoryModel.testCategories())
        dataSource.apply(snap)
    }

    func makeDataSource() -> DataSource {
        
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self]collection, indexPath, item in
            
            let cell = collection.dequeueReusableCell(withReuseIdentifier: HomeMovieCollectionViewCell.id, for: indexPath)
            configureCell(cell, item)
            return cell
        }
        
        source.supplementaryViewProvider = {
            [unowned self]collection, kind, indexPath in
            
            let header = collection.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: MovieCategoryCollectionReusableView.id,
                for: indexPath)
            configureHeader(header, indexPath)
            return header
        }

        return source
    }
    
    private func configureCell(_ cell : UICollectionViewCell, _ item : MovieModel) {
        
        guard let cell = cell as? HomeMovieCollectionViewCell else { return }
        
        cell.buttonDelegate = self
        cell.cellID = item.id
        cell.dateLabel.text = dateFormatter.string(from: item.releaseDate)
        cell.nameLabel.text = item.name
    }
    
    private func configureHeader(_ header : UICollectionReusableView, _ indexPath : IndexPath) {
        
        guard let header = header as? MovieCategoryCollectionReusableView else { return }
        
        let item = dataSource.sectionIdentifier(for: indexPath.section)

        header.buttonDelegate = self
        header.section = indexPath.section
        header.categoryLabel.text = item?.name ?? ""
        updateHeaderIcon(indexPath.section, header)
    }
    
    private func updateHeaderIcon(_ section : Int, _ headerCell : MovieCategoryCollectionReusableView? = nil) {

        let header = headerCell == nil
        ? contentView.collectionView.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath(row: 0, section: section)) as? MovieCategoryCollectionReusableView
        : headerCell
        let sectionItem = dataSource.snapshot().sectionIdentifiers[section]
        let sectionIsExpanded = dataSource.snapshot().numberOfItems(inSection: sectionItem) > 0
        
        header?.iconImageView.image = sectionIsExpanded ? MovieCategoryCollectionReusableView.arrowUp : MovieCategoryCollectionReusableView.arrowDown
    }
    
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    
}


extension HomeViewController : HomeCellButtonDelegate {
    
    func sectionPressed(_ section: Int) {

        if isSectionExpanded(section) {
            collapseSection(section)
        } else {
            expandSection(section)
        }
        
       updateHeaderIcon(section)
    }
    
    func moviePressed(_ id: UUID) {
        print("\(id)")
    }
    
    private func isSectionExpanded(_ int : Int) -> Bool {
        
        guard let sectionItem = dataSource.sectionIdentifier(for: int) else { return false }
        
        let snap = dataSource.snapshot()
        let numOfItems = snap.numberOfItems(inSection: sectionItem)
        
        return numOfItems > 0
    }
    
    private func expandSection(_ section : Int) {
        
        guard let sectionItem = dataSource.sectionIdentifier(for: section) else { return }
        
        var snap = dataSource.snapshot()
        snap.appendItems(MovieModel.fakeData(3), toSection: sectionItem)
        dataSource.apply(snap)
    }
    
    private func collapseSection(_ section : Int) {
        
        guard let sectionItem = dataSource.sectionIdentifier(for: section) else { return }

        var snap = dataSource.snapshot()
        snap.deleteItems(snap.itemIdentifiers(inSection: sectionItem))
        dataSource.apply(snap)
    }
}
