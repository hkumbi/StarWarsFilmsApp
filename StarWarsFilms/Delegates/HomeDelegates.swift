//
//  HomeDelegates.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-04-05.
//

import Foundation
import UIKit


protocol HomeCellButtonDelegate : AnyObject {
    func sectionPressed(_ section : Int)
    func moviePressed(_ id: UUID)
}
