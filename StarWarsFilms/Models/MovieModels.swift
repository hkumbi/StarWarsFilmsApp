//
//  MovieModel.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-03-28.
//

import Foundation
import UIKit


class MovieModel: Hashable, Equatable {
    
    static func == (lhs: MovieModel, rhs: MovieModel) -> Bool {
        lhs.id == rhs.id
    }
    
    static func fakeData(_ numOfItems : Int) -> [MovieModel] {
        
        var items : [MovieModel] = []
        
        for _ in 0...numOfItems {
            items.append(MovieModel(name: "Movie", releaseDate: Date.getDate(5, 1, 2000)))
        }
        
        return items
    }
    
    let id = UUID()
    var name : String = ""
    var releaseDate : Date!
    
    init(name: String, releaseDate: Date!) {
        self.name = name
        self.releaseDate = releaseDate
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
        hasher.combine(releaseDate)
    }
}


class MovieCategoryModel: Hashable, Equatable {
    
    static func == (lhs: MovieCategoryModel, rhs: MovieCategoryModel) -> Bool {
        lhs.id == rhs.id
    }
    
    static func testCategories() -> [MovieCategoryModel] {
        let original = MovieCategoryModel(name: "Original Trilogy")
        let prequel = MovieCategoryModel(name: "Prequel Trilogy")
        let sequel = MovieCategoryModel(name: "Sequel Trilogy")
        let other = MovieCategoryModel(name: "Others")
        
        return [original, prequel, sequel, other]
    }
    
    let id = UUID()
    var name : String = ""
    var image : UIImage?
    
    init(name: String, image: UIImage? = nil) {
        self.name = name
        self.image = image
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
        hasher.combine(image)
    }
}
