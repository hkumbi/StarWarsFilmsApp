//
//  MovieCategoryCollectionReusableView.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-04-02.
//

import UIKit

class MovieCategoryCollectionReusableView: UICollectionReusableView {
    
    static let id = "MovieCategoryCollectionReusableView"
    static let size = CGSize(width: UIScreen.width*0.9, height: height)
    static let arrowUp = UIImage(systemName: "chevron.up", withConfiguration: UIImage.SymbolConfiguration(weight: .semibold))
    static let arrowDown = UIImage(systemName: "chevron.down", withConfiguration: UIImage.SymbolConfiguration(weight: .semibold))
    
    private static let height : CGFloat = 90
    
    let button : UIButton = .preppedForAutoLayout()
    let categoryLabel : UILabel = .preppedForAutoLayout()
    let iconImageView : UIImageView = .preppedForAutoLayout()
    
    var section : Int!
    unowned var buttonDelegate : HomeCellButtonDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
        setUpGestures()
        
        self.addSubview(button)
        self.addSubview(categoryLabel)
        self.addSubview(iconImageView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews() {
        
        button.setImage(UIImage(named: "venti"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.clipsToBounds = true
        button.layer.cornerRadius = Self.height/2
        
        categoryLabel.textColor = .white
        categoryLabel.font = .systemFont(ofSize: 20, weight: .bold)
        
        iconImageView.image = UIImage(systemName: "chevron.down", withConfiguration: UIImage.SymbolConfiguration(weight: .semibold))
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.tintColor = .white
    }
    
    private func setUpGestures() {
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        button.pinImageView()
                
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalTo: heightAnchor),
            button.centerXAnchor.constraint(equalTo: centerXAnchor),
            button.widthAnchor.constraint(equalToConstant: Self.size.width),
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            categoryLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            categoryLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            
            iconImageView.widthAnchor.constraint(equalToConstant: 20),
            iconImageView.heightAnchor.constraint(equalToConstant: 20),
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImageView.rightAnchor.constraint(equalTo: button.rightAnchor, constant: -20),
        ])
    }
    
    @objc private func buttonPressed() {
        buttonDelegate.sectionPressed(section)
    }
}
