//
//  HomeMovieCollectionViewCell.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-04-02.
//

import UIKit

class HomeMovieCollectionViewCell: UICollectionViewCell {
    
    static let id = "HomeMovieCollectionViewCell"
    static let size = CGSize(width: UIScreen.width*0.7, height: height)
    
    private static let height : CGFloat = 70
    
    let button : UIButton = .preppedForAutoLayout()
    let nameLabel : UILabel = .preppedForAutoLayout()
    let dateLabel : UILabel = .preppedForAutoLayout()
    
    var cellID : UUID!
    unowned var buttonDelegate : HomeCellButtonDelegate!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
        setUpGestures()
        
        self.contentView.addSubview(button)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(dateLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews() {
        
        nameLabel.textAlignment = .left
        nameLabel.font = .systemFont(ofSize: 14)
        
        dateLabel.font = .systemFont(ofSize: 12, weight: .medium)
        dateLabel.textColor = .gray
        
        contentView.backgroundColor = .black.withAlphaComponent(0.05)
        contentView.layer.cornerRadius = Self.height/2
    }
    
    private func setUpGestures() {
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        button.pinTo(contentView)
        
        NSLayoutConstraint.activate([
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15),
            nameLabel.rightAnchor.constraint(equalTo: dateLabel.leftAnchor, constant: -15),
            
            dateLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            dateLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15),
        ])
    }
    
    @objc private func buttonPressed() {
        buttonDelegate.moviePressed(cellID)
    }
    
}
