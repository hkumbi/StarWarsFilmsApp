//
//  HomeView.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-03-28.
//

import UIKit

class HomeView: UIView {
    
    let titleLabel : UILabel = .preppedForAutoLayout()
    let collectionView : UICollectionView = .preppedForAutoLayout()
    

    init() {
        super.init(frame: .zero)
        
        setUpViews()
        
        self.addSubview(titleLabel)
        self.addSubview(collectionView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToTopSafeArea(superview)
    }
    
    private func setUpViews() {
        
        titleLabel.text = "Select A Movie"
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.font = .systemFont(ofSize: 18, weight: .medium)
        
        collectionView.alwaysBounceVertical = true
    }
    
    private func makeConstraints() {
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

}
