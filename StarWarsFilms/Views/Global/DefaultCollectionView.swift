//
//  DefaultCollectionView.swift
//  StarWarsFilms
//
//  Created by Herve Kumbi on 2023-03-28.
//

import UIKit

class DefaultCollectionView: UICollectionView {

    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        
        setUpViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        view is UIButton ? true : super.touchesShouldCancel(in: view)
    }
    
    private func setUpViews() {
        backgroundColor = .white
    }

}
